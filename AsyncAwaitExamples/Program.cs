﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwaitExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = AccessTheWebAsync();
            for (int i = 0; i < 100; i++)
            {
                Task.Delay(100);
                Console.WriteLine($"Do work in Main thread {i}");
            }
            Console.Read();
        }

        static async Task<int> AccessTheWebAsync()
        {
            string urlContents = String.Empty;
            using (HttpClient client = new HttpClient())
            {
                Task<string> getStringTask = client.GetStringAsync("http://www.google.ca");
                DoIndependentWork();
                urlContents = await getStringTask;
                Console.WriteLine(urlContents);
            }
            return urlContents.Length;
        }

        static void DoIndependentWork()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine($"Do indepentdent work {i}");
            }

        }
    }
}
