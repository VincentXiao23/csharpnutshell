﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncExample2
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Start downloading...");
            string url = @"http://www.baidu.com";

            // b.
            DownLoadAsync(url);

            // The above method won't block, it will continue to execute
            while (true)
            {
                Console.WriteLine("Working on other stuff...");
                Thread.Sleep(1000);
            }
        }

        public static async void DownLoadAsync(string url)
        {
            string content = string.Empty;
            using (HttpClient client = new HttpClient())
            {
                // This method awaits, return to Main method, after GetStringAsync() is finished, it executes the statements after await. When it finishes, return to Main method again.
                content = await client.GetStringAsync(url);
            }

            // a. This is like a call back function.
            Console.WriteLine("End downloading...");
            Console.WriteLine(content);
        }
    }
}
