﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadPoolExamples
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Main thread does some work then exits.");
            // Queue the task
            Thread newThread = new Thread(ThreadProcedure2);
            newThread.Start();
            ThreadPool.QueueUserWorkItem(ThreadProcedure);
            Thread.Sleep(1000);
            Console.WriteLine("Main Thread done.");
        }

        static void ThreadProcedure(Object stateInfo)
        {
            Console.WriteLine("Hello from the thread pool.");
            Thread.Sleep(100000);
            Console.WriteLine("It SHOULD END if main thread ends, so this line will not be printed.");
        }
        
        static void ThreadProcedure2(Object stateInfo)
        {
            Console.WriteLine("Hello from the new thread 22222.");
            Thread.Sleep(5000);
            Console.WriteLine("It should NOT END if main thread ends, so this line will be printed.");
        }
    }
}