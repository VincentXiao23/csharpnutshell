﻿using System;
using System.Threading;

namespace ThreadProject
{
    class Program
    {
        static void Main(string[] args)
        {
            // Thread Test 01
//            Thread t = new Thread(WriteY);
//            t.Start();
//            
//            for(int i = 0; i < 10000; i++) Console.Write("-");
            

            // Thread Test 02 
            // ThreadTest02();
            
            
            // Thread Test 03
            // ThreadTest03();
            
            // Thread Test 04
            ThreadTest04();
        }

        static void ThreadTest04()
        {
            bool done = false;
            ThreadStart action = () =>
            {
                if (!done)
                {
                    done = true;
                    Console.WriteLine("Done");
                }
            };
            new Thread(action).Start();
            action();
        }

        static void ThreadTest03()
        {
            Program tt = new Program();
            new Thread(tt.Do).Start();
            tt.Do();
        }

        bool _done;
        void Do()
        {
            if (!_done)
            {
                _done = true;
                Console.WriteLine("done");
            }
        }

        static void WriteY()
        {
            for (int i = 0; i < 10000; i++)
            {
                Console.Write("Y");
            }
        }

        static void ThreadTest02()
        {
            new Thread(Go).Start();
            Go();
        }

        static void Go()
        {
            for (int cycle = 0; cycle < 5; cycle++)
            {
                Console.WriteLine(cycle);
            }
        }
    }
}